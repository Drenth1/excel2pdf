# /*
# | ------------------------------------------------
# | Excel2PDF
# | ------------------------------------------------
# |
# | Script om Excel bestanden per worksheet om te
# | zetten naar PDF-bestanden.
# |
# */

Write-Host "Starting..."

$TerminalWindow = '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);'
Add-Type -Name Win -Member $TerminalWindow -Namespace Native
[Native.Win]::ShowWindow(([System.Diagnostics.Process]::GetCurrentProcess() | Get-Process).MainWindowHandle, 0)

Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

$EXCEL2PDF = New-Object System.Windows.Forms.Form
$EXCEL2PDF.ClientSize = New-Object System.Drawing.Point(800,300)
$EXCEL2PDF.Text = "Excel naar PDF converter"
$EXCEL2PDF.BackColor = [System.Drawing.ColorTranslator]::FromHtml("#dfe6e9")
$EXCEL2PDF.TopMost = $false
$EXCEL2PDF.MaximizeBox = $false
$EXCEL2PDF.FormBorderStyle = "FixedSingle"
$EXCEL2PDF.StartPosition = "CenterScreen"

$IntroTitle = New-Object System.Windows.Forms.Label
$IntroTitle.Text = "Excel2PDF"
$IntroTitle.Width = 250
$IntroTitle.Height = 30
$IntroTitle.Location = New-Object System.Drawing.Point(18,20)
$IntroTitle.Font = New-Object System.Drawing.Font('Segoe UI',14)

$IntroDescription = New-Object System.Windows.Forms.Label
$IntroDescription.Text = "Deze applicatie zet beoordelingsformulieren om naar PDF-bestanden, per werkproces."
$IntroDescription.AutoSize = $true
$IntroDescription.Width = 25
$IntroDescription.Height = 10
$IntroDescription.Location = New-Object System.Drawing.Point(20,50)
$IntroDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)

$FileSettingsGroupBox = New-Object System.Windows.Forms.Groupbox
$FileSettingsGroupBox.Height = 115 
$FileSettingsGroupBox.Width = 515
$FileSettingsGroupBox.Text = "Bestandsinstellingen"
$FileSettingsGroupBox.Location = New-Object System.Drawing.Point(20,80)

$SourceFolderInput = New-Object System.Windows.Forms.Label
$SourceFolderInput.Text = "Bronmap met excelbestanden"
$SourceFolderInput.AutoSize = $true
$SourceFolderInput.Width = 25
$SourceFolderInput.Height = 8
$SourceFolderInput.Location = New-Object System.Drawing.Point(5,15)
$SourceFolderInput.Font = New-Object System.Drawing.Font('Segoe UI',10)

$SourceFolderInputButton = New-Object System.Windows.Forms.Button
$SourceFolderInputButton.Text = "Selecteer bronmap"
$SourceFolderInputButton.Width = 120
$SourceFolderInputButton.Height = 35
$SourceFolderInputButton.Location = New-Object System.Drawing.Point(375,15)
$SourceFolderInputButton.Font = New-Object System.Drawing.Font('Segoe UI',8)
$SourceFolderInputButton.Add_Click({ SelectSourceFolder })

$CurrentlySelectedFilePath = New-Object System.Windows.Forms.Label
$CurrentlySelectedFilePath.Text = $PSScriptRoot
$CurrentlySelectedFilePath.AutoSize = $false
$CurrentlySelectedFilePath.Width = 350
$CurrentlySelectedFilePath.Height = 30
$CurrentlySelectedFilePath.Location = New-Object System.Drawing.Point(5,35)
$CurrentlySelectedFilePath.Font = New-Object System.Drawing.Font('Segoe UI',8,[System.Drawing.FontStyle]([System.Drawing.FontStyle]::Italic))

$NamebaseDescription = New-Object System.Windows.Forms.Label
$NamebaseDescription.Text = "Bestandsnaam van export baseren op"
$NamebaseDescription.AutoSize = $true
$NamebaseDescription.Width = 25
$NamebaseDescription.Height = 10
$NamebaseDescription.Location = New-Object System.Drawing.Point(5,65)
$NamebaseDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)

$NamebaseSelection = New-Object System.Windows.Forms.ComboBox
$NamebaseSelection.Text = "Selecteer"
$NamebaseSelection.Width = 120
$NamebaseSelection.Height = 20
$NamebaseSelection.Items.AddRange(@('Bronbestand','Mapnaam'))
$NamebaseSelection.Location = New-Object System.Drawing.Point(375,60)
$NamebaseSelection.Font = New-Object System.Drawing.Font('Segoe UI',10)
$NamebaseSelection.SelectedIndex = 0

$DelimiterDescription = New-Object System.Windows.Forms.Label
$DelimiterDescription.Text = "Scheidingsteken in bestandsnaam of mapnaam"
$DelimiterDescription.AutoSize = $true
$DelimiterDescription.Width  = 25
$DelimiterDescription.Height = 10
$DelimiterDescription.Location = New-Object System.Drawing.Point(5,90)
$DelimiterDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)

$DelimiterSelection = New-Object System.Windows.Forms.ComboBox
$DelimiterSelection.Text = "Selecteer"
$DelimiterSelection.Width = 120
$DelimiterSelection.Height = 20
$DelimiterSelection.Items.AddRange(@('_', '-', '.', ' '))
$DelimiterSelection.Location = New-Object System.Drawing.Point(375,85)
$DelimiterSelection.Font = New-Object System.Drawing.Font('Segoe UI',10)

$NameComponentsComboBox = New-Object System.Windows.Forms.Groupbox
$NameComponentsComboBox.Height = 115
$NameComponentsComboBox.Width = 230
$NameComponentsComboBox.Text = "Naam onderdelen"
$NameComponentsComboBox.Location = New-Object System.Drawing.Point(540,80)

$FirstnameNamecomponentDescription = New-Object System.Windows.Forms.Label
$FirstnameNamecomponentDescription.Text = "Voornaam"
$FirstnameNamecomponentDescription.AutoSize = $true
$FirstnameNamecomponentDescription.Width = 25
$FirstnameNamecomponentDescription.Height = 10
$FirstnameNamecomponentDescription.Location = New-Object System.Drawing.Point(5,15)
$FirstnameNamecomponentDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)

$FirstnameNamecomponent = New-Object System.Windows.Forms.TextBox
$FirstnameNamecomponent.Multiline = $false
$FirstnameNamecomponent.Text = "2"
$FirstnameNamecomponent.Width = 30
$FirstnameNamecomponent.Height = 20
$FirstnameNamecomponent.Location = New-Object System.Drawing.Point(190,10)
$FirstnameNamecomponent.Font = New-Object System.Drawing.Font('Segoe UI',10)

$LastnameNamecomponentDescription = New-Object System.Windows.Forms.Label
$LastnameNamecomponentDescription.Text = "Achternaam"
$LastnameNamecomponentDescription.AutoSize = $true
$LastnameNamecomponentDescription.Width = 25
$LastnameNamecomponentDescription.Height = 10
$LastnameNamecomponentDescription.Location = New-Object System.Drawing.Point(5,40)
$LastnameNamecomponentDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)

$LastnameNamecomponent = New-Object System.Windows.Forms.TextBox
$LastnameNamecomponent.Multiline = $false
$LastnameNamecomponent.Text = "1"
$LastnameNamecomponent.Width = 30
$LastnameNamecomponent.Height = 20
$LastnameNamecomponent.Location = New-Object System.Drawing.Point(190,35)
$LastnameNamecomponent.Font = New-Object System.Drawing.Font('Segoe UI',10)

$StudentnumberNamecomponentDescription = New-Object System.Windows.Forms.Label
$StudentnumberNamecomponentDescription.text = "Studentnummer"
$StudentnumberNamecomponentDescription.AutoSize = $true
$StudentnumberNamecomponentDescription.width = 25
$StudentnumberNamecomponentDescription.height = 10
$StudentnumberNamecomponentDescription.Location = New-Object System.Drawing.Point(5,65)
$StudentnumberNamecomponentDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)

$StudentnumbernameComponent = New-Object System.Windows.Forms.TextBox
$StudentnumbernameComponent.Multiline = $false
$StudentnumbernameComponent.Text = "3"
$StudentnumbernameComponent.Width = 30
$StudentnumbernameComponent.Height = 20
$StudentnumbernameComponent.Location = New-Object System.Drawing.Point(190,60)
$StudentnumbernameComponent.Font = New-Object System.Drawing.Font('Segoe UI',10)

$DefaultNamecomponentcountDescription = New-Object System.Windows.Forms.Label
$DefaultNamecomponentcountDescription.Text = "Standaard aantal onderdelen"
$DefaultNamecomponentcountDescription.AutoSize = $true
$DefaultNamecomponentcountDescription.Width = 25
$DefaultNamecomponentcountDescription.Height = 10
$DefaultNamecomponentcountDescription.Location = New-Object System.Drawing.Point(5,90)
$DefaultNamecomponentcountDescription.Font = New-Object System.Drawing.Font('Segoe UI',10)
$DefaultNamecomponentcountDescription.Visible = $false

$DefaultNamecomponentCount = New-Object System.Windows.Forms.TextBox
$DefaultNamecomponentCount.Multiline = $false
$DefaultNamecomponentCount.Text = "4"
$DefaultNamecomponentCount.Width = 30
$DefaultNamecomponentCount.Height = 20
$DefaultNamecomponentCount.Location = New-Object System.Drawing.Point(190,85)
$DefaultNamecomponentCount.Font = New-Object System.Drawing.Font('Segoe UI',10)
$DefaultNamecomponentCount.Visible = $false

$StartButton = New-Object System.Windows.Forms.Button
$StartButton.text = "Start export"
$StartButton.width = 230
$StartButton.height = 30
$StartButton.location = New-Object System.Drawing.Point(540,200)
$StartButton.Font = New-Object System.Drawing.Font('Segoe UI',10)
$StartButton.Add_Click({ ExportWorkSheetsToPDF })

$ExportProgressBar = New-Object System.Windows.Forms.ProgressBar
$ExportProgressBar.width = 516
$ExportProgressBar.height = 30
$ExportProgressBar.location = New-Object System.Drawing.Point(20,200)
$ExportProgressBar.Minimum = 0

$ExportStatusIndicator = New-Object System.Windows.Forms.Label
$ExportStatusIndicator.Text = "Status: Wachtend op exportopdracht."
$ExportStatusIndicator.AutoSize = $false
$ExportStatusIndicator.Location = New-Object System.Drawing.Point(20,245)
$ExportStatusIndicator.Font = New-Object System.Drawing.Font('Segoe UI',10)
$ExportStatusIndicator.Width = 745

$CountProgressIndicator = New-Object System.Windows.Forms.Label
$CountProgressIndicator.Text = "Bestanden omgezet: 0/0"
$CountProgressIndicator.AutoSize = $true
$CountProgressIndicator.Width = 25
$CountProgressIndicator.Height = 10
$CountProgressIndicator.Location = New-Object System.Drawing.Point(20,270)
$CountProgressIndicator.Font = New-Object System.Drawing.Font('Segoe UI',10)
$CountProgressIndicator.Visible = $false

$EXCEL2PDF.controls.AddRange(@($IntroTitle,$IntroDescription,$FileSettingsGroupBox,$NameComponentsComboBox,$StartButton,$ExportProgressBar,$ExportStatusIndicator,$CountProgressIndicator))
$FileSettingsGroupBox.controls.AddRange(@($SourceFolderInput,$SourceFolderInputButton,$CurrentlySelectedFilePath,$NamebaseDescription,$NamebaseSelection,$DelimiterDescription,$DelimiterSelection))
$NameComponentsComboBox.controls.AddRange(@($FirstnameNamecomponentDescription,$FirstnameNamecomponent,$LastnameNamecomponentDescription,$LastnameNamecomponent,$StudentnumberNamecomponentDescription,$StudentnumbernameComponent,$DefaultNamecomponentcountDescription,$DefaultNamecomponentCount))

# This is the logic of the application.
# Define functions or other code here.
function SelectSourceFolder {
    # De assembly voor de file explorer hoeft niet ingeladen te worden.
    # Deze is al ingeladen aan de top van het script omdat de GUI deze ook nodig heeft.
    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog
    $foldername.Description = "Selecteer een map waarin de Excel-bestanden of mappen met Excel-bestanden staan."
    $foldername.rootfolder = "MyComputer"
    $foldername.SelectedPath = $initialDirectory

    # Als de gebruiker een map geselecteerd heeft slaan we deze op,
    # wanneer dit niet zo is, resetten we de string naar null.
    if($foldername.ShowDialog() -eq "OK")
    {
        $folder += $foldername.SelectedPath
        $CurrentlySelectedFilePath.Text = $folder
    }
    else 
    {
        $folder = $null   
        $CurrentlySelectedFilePath.Text = "Geen map geselecteerd." 
    }

    return $folder
}

function ExportWorkSheetsToPDF {
    $ExcelDirectory = $CurrentlySelectedFilePath.Text

    $Formats = "Microsoft.Office.Interop.Excel.xlFixedFormatType" -as [type]

    $Excel = New-Object -ComObject Excel.Application
    $Excel.Visible = $false

    $ExcelFiles = Get-ChildItem $ExcelDirectory -Filter "*.xlsx" -Recurse

    $FileCountToConvert = $ExcelFiles.Count

    $ExportProgressBar.Value = 0
    $ExportProgressBar.Maximum = $FileCountToConvert

    $CountProgressIndicator.Text = "Bestanden omgezet: 0/$FileCountToConvert"
    $CountProgressIndicator.Visible = $true

    $Iterator = 0

    foreach($ExcelFile in $ExcelFiles)
    {
        $Iterator++
        $Workbook = $Excel.Workbooks.Open($ExcelFile.FullName)
        $Workbook.Saved = $true

        if($NamebaseSelection.Text -eq "Bronbestand")
        {
            # Neem de naam van het bestand, splits deze eerst op de punt om de bestandsextensie weg te halen.
            # Vervolgens splitsen op het teken dat de gebruiker heeft aangegeven.
            $Namecomponents = $Workbook.Name.Split(".")[0].Split($DelimiterSelection.Text)
        }
        elseif ($NamebaseSelection.Text -eq "Mapnaam") 
        {
            # Neem de map van het bestand waarin het Excel-bestand staat. Splits deze op de backslash en neem het laatste element.
            # Vervolgens splitsen op het teken dat de gebruiker heeft aangegeven.
            $Namecomponents = (Get-Item $Workbook.FullName).DirectoryName.Split("\")[-1].Split($DelimiterSelection.Text)
        } 

        $CountProgressIndicator.Text = "Bestanden omgezet: $Iterator/$FileCountToConvert"

        foreach($Worksheet in $Workbook.Worksheets)
        {

            if(($Worksheet.Name -eq "Voorblad") -or ([int]$Worksheet.Visible -ne -1))
            {
                continue
            }

            Write-Host $Worksheet.PageSetup.Pages

            $Firstname = $Namecomponents[[int]$FirstnameNamecomponent.Text - 1]
            $Lastname = $Namecomponents[[int]$LastnameNamecomponent.Text - 1]
            $Studentnumber = $Namecomponents[[int]$StudentnumberNamecomponent.Text - 1]

            $SheetName = @($Lastname, $Firstname, $Studentnumber, $Worksheet.Name)
            $SheetPrettyName = $SheetName -join " "

            $OutputPath = "$ExcelDirectory\" + $SheetPrettyName + ".pdf"

            $ExportStatusIndicator.Text = "Status: $SheetPrettyName aan het exporteren."

            $Worksheet.ExportAsFixedFormat($Formats::xlTypePDF, $OutputPath)
        }

        $ExportProgressBar.Value = $Iterator
        $Excel.Workbooks.Close()
    }

    $Excel.Quit()
    
    $ExportStatusIndicator.Text = "Status: export voltooid."
}

# Form elements and logic have been defined.
# Render the form.
[void]$EXCEL2PDF.ShowDialog()
